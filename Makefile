COMMANDS := $(MAKEFILE_LIST)

# Check if container-structure-test is present
CONTAINER_TEST := $(shell command -v container-structure-test 2>/dev/null)

.DEFAULT_GOAL := build

build:
	@shef build install.recipe build/install

install:
	@/bin/bash build/install

test: build-ci test-container
build-ci:
	@docker-compose -f .docker/docker-compose.build.yml build rofi-recipe
test-container:
ifndef CONTAINER_TEST
	@echo "container-structure-test is not available. Follow instructions here : https://github.com/GoogleContainerTools/container-structure-test"
	exit 1
endif

	@container-structure-test test --image registry.gitlab.com/leonardmessier/shef/recipes/rofi-recipe --config tests/acceptance/features.yml --quiet
	@echo "\033[32m✓\033[0m Acceptance tests passed"

.PHONY: install build test
