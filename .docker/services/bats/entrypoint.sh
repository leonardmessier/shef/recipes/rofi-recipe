#!/bin/bash

if [ "$1" == 'bash' ]; then
  exec /bin/bash
  exit "$@"
fi

bats /recipe/tests
